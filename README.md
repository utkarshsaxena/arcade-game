# README #

## Introduction:  

My first arcade game from scratch - **Flying Caveman** 

## Technologies: ##

* Python
* Pygame Library

## Purpose: ##

Learn and apply Python Programming Language to create an application.

## Procedure: ##

1. Install Python. ( https://www.python.org/ )
2. Install the Pygame library. ( http://pygame.org/ )
4. Execute/Run the file "#custom input box #workingFinal.py"
5. Enter password: apple

## Timestamp: ##

**May 2014 – July 2014**