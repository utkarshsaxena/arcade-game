import pygame, os
import pygame, pygame.font, pygame.event, pygame.draw, string
from pygame.locals import *
from random import randint

# Define some colors
BLACK    = (   0,   0,   0)
WHITE    = ( 255, 255, 255)
GREEN    = (   0, 255,   0)
RED      = ( 255,   0,   0)
BLUE     = ( 0,     0,  255)
UNKNOWN  = (190,200,90)
pygame.init()
 
# Set the width and height of the screen [width, height]

size = (800, 500)
screen = pygame.display.set_mode(size)

# Set Background
background_image = pygame.image.load("continous_background.png").convert()

pygame.display.set_caption("Flying Caveman >> Tarzan")

# Set Background music
pygame.mixer.music.load("background_music.wav")
pygame.mixer.music.set_endevent(pygame.constants.USEREVENT)
pygame.mixer.music.play()

# Other sound files. You can play the sound using hit_sound.play()
fall_sound = pygame.mixer.Sound("fall_sound.wav")
hit_sound = pygame.mixer.Sound("hit_sound.wav") 
jump_sound = pygame.mixer.Sound("jump_sound.wav")
fire_sound = pygame.mixer.Sound("fire_sound.wav")
##################################################### Code from online source below: 

def get_key():
  while 1:
    event = pygame.event.poll()
    if event.type == KEYDOWN:
      return event.key
    else:
      pass
    
def display_box(screen, message):
  "Print a message in a box in the middle of the screen"
  fontobject = pygame.font.Font(None,18)
  pygame.draw.rect(screen, (0,0,0),
                   ((screen.get_width() / 2) - 100,
                    (screen.get_height() / 2) - 10,
                    200,20), 0)
  pygame.draw.rect(screen, (255,255,255),
                   ((screen.get_width() / 2) - 102,
                    (screen.get_height() / 2) - 12,
                    204,24), 1)
  if len(message) != 0:
    screen.blit(fontobject.render(message, 1, (255,255,255)),
                ((screen.get_width() / 2) - 100, (screen.get_height() / 2) - 10))
  pygame.display.flip()

def ask(screen, question):
  "ask(screen, question) -> answer"
  pygame.font.init()
  current_string = []
  display_box(screen, question + ": " + string.join(current_string,""))
  while 1:
    inkey = get_key()
    if inkey == K_BACKSPACE:
      current_string = current_string[0:-1]
    elif inkey == K_RETURN:
      break
    elif inkey == K_MINUS:
      current_string.append("-")
    elif inkey <= 127:
      current_string.append(chr(inkey))
    display_box(screen, question + ": " + string.join(current_string,""))
    
  return string.join(current_string,"")
####################################################################################

##################################### My Code below:
def game_end(score, time):  
  for i in range(400):
    i += 1
    pygame.draw.circle(screen, BLACK, [400,250],30 + i, 0)
    pygame.draw.circle(screen, (15,100,220), [400,250],15 + i, 0)
    pygame.draw.circle(screen, WHITE, [400,250],i , 0)
    pygame.time.wait(15)
    font = pygame.font.SysFont('Calibri', 35, True, False)
    text = font.render("Game Over!!! Final Score: " + str(score) + " Total Time: " + str(time),True,RED)
    screen.blit(text, [100, 170])    
    if i > 290:
      font = pygame.font.SysFont('Calibri', 35, True, False)
      text1 = font.render("Press P to play again",True,GREEN)
      screen.blit(text1, [150, 230])
      pygame.display.flip()
    else:
      pygame.display.flip()
  platform1.reset_pos()
  platform2.reset_pos()
  platform3.reset_pos()
  platform4.reset_pos()
  #platform5.reset_pos()  
  #pygame.time.wait(500)
  
# Setting objects 
class Platform(pygame.sprite.Sprite): #Platform block - NOTE: Platform 5 commented out

    def __init__(self):
        pygame.sprite.Sprite.__init__(self)

        self.image = pygame.image.load("wood_platform.png").convert()
        self.image.set_colorkey(WHITE)
        

        self.rect = self.image.get_rect()
        
        """self.image = pygame.Surface([130, 40])
        self.image.fill(UNKNOWN)
        self.image.set_alpha(175)
        

        self.rect = self.image.get_rect()"""

    def reset_pos(self):
        self.rect.x = randint(100, 700)
        self.rect.y = randint(100, 450)

                
class Block(pygame.sprite.Sprite): #Chicken Block

    def __init__(self, color, width, height):
        pygame.sprite.Sprite.__init__(self)

        self.image = pygame.image.load("food.png").convert()
        self.image.set_colorkey(WHITE)
        

        self.rect = self.image.get_rect()

    def reset_pos(self):

        self.rect.x = randint(820, 1000)
        self.rect.y = randint(0, 500)

    def update(self):

        self.rect.x -= 6

        if self.rect.x < -50 :
            self.reset_pos()

class Block1(pygame.sprite.Sprite): #Player block

    def __init__(self,  width, height):
        pygame.sprite.Sprite.__init__(self)

        
        self.image = pygame.Surface([width, height])
        self.image.set_colorkey(BLACK)
        self.rect = self.image.get_rect()

    def reset_pos(self):

        self.rect.x = randint(900, 1300)
        self.rect.y = randint(0, 500)

    def update(self):

        self.rect.x -= 1

        if self.rect.x < 0:
            self.reset_pos()
            
class Flame(pygame.sprite.Sprite):

    def __init__(self, color, width, height):
        pygame.sprite.Sprite.__init__(self)
        
        self.image = pygame.image.load("flame.png").convert()
        self.image.set_colorkey(WHITE)
        

        self.rect = self.image.get_rect()

    def reset_pos(self):
        self.rect.x = randint(0, 800)
        self.rect.y = randint(-300, -150)

    def update(self):

        self.rect.y += 1

        if self.rect.y > 600:
            self.reset_pos()

class Player(Block1):

    x_player = 5
    y_player = 375
    x_playerSpeed = 0
    y_playerSpeed = 0
    # List of sprites we can bump against
    level = None

    #Get player image
    player_image = pygame.image.load("player.png").convert()
    player_image.set_colorkey(WHITE)
    
    def update(self):
        
        boy.x_player += boy.x_playerSpeed
        boy.y_player += boy.y_playerSpeed
        
        
    def jump(self):
        if (Player.y_player + 125) >= 0 :
            self.y_playerSpeed = -5
            jump_sound.play()

    def fall(self):
        if (Player.y_player + 125) >= 0 :
            self.y_playerSpeed = 4
            fall_sound.play()


# Initializing variables            
game_score = 0
health_status = 0
done = False

# This is a list of sprites. Each block in the program is added to this list.
block_list = pygame.sprite.Group()
block_list_flame = pygame.sprite.Group()
block_list_platform = pygame.sprite.Group()
  
# This is a list of every sprite
all_sprites_list = pygame.sprite.Group()

# Creating blocks, fire and platform at random
for i in range(4):
    # This represents a block
    block = Block(WHITE, 90, 40)
    
  
    # Set a random location for the block
    block.rect.x = randint(0, 800)
    block.rect.y = randint(0, 500)
      
    # Add the block to the list of objects
    block_list.add(block)
    all_sprites_list.add(block)
    

for j in range(4):
    # This represents a block of Flame
    flame = Flame(WHITE, 31, 43)
    
    # Set a random location for the block
    flame.rect.x = randint(0, 800)
    flame.rect.y = randint(0, 500)
    
    all_sprites_list.add(flame)
    block_list_flame.add(flame)

for k in range(1):
    # This represents a Platform
    platform1 = Platform()
    platform2 = Platform()
    platform3 = Platform()
    platform4 = Platform()
    #platform5 = Platform()

    #Set a random location for the block
    platform1.rect.x = randint(100, 400)
    platform1.rect.y = randint(100, 275)
    
    # Set a random location for the block
    platform2.rect.x = randint(400, 700)
    platform2.rect.y = randint(100, 275)
    
    # Set a random location for the block
    platform3.rect.x = randint(400, 700)
    platform3.rect.y = randint(275, 450)
    
    # Set a random location for the block
    platform4.rect.x = randint(100, 400)
    platform4.rect.y = randint(275, 450)
    
    # Set a random location for the block
    #platform5.rect.x = randint(400, 700)
    #platform5.rect.y = randint(275, 450)
        
    
    all_sprites_list.add(platform1)
    all_sprites_list.add(platform2)
    all_sprites_list.add(platform3)
    all_sprites_list.add(platform4)
    #all_sprites_list.add(platform5)
    block_list_platform.add(platform1)
    block_list_platform.add(platform2)
    block_list_platform.add(platform3)
    block_list_platform.add(platform4)
    #block_list_platform.add(platform5)  

# Setting some more parameters    
boy = Player(84,94)
all_sprites_list.add(boy)
text = " "
textScore = " "
x_background = 0
x_backgroundSpeed = -4
y_background = 0
y_backgroundSpeed = 3
switch_count = 0
# manages how fast the screen updates
clock = pygame.time.Clock()
toggle_pause = 1
screen = pygame.display.set_mode((800,500))
answer =  ask(screen, "Password")
game_title_image = pygame.image.load("game_title.png").convert()
game_title_image.set_colorkey(WHITE)
screen.blit(game_title_image, [200,50])


if answer == "apple":
    font = pygame.font.SysFont('Calibri', 30, True, False)
    text = font.render("Press P to play",True,(0,255,0))
    screen.blit(text, [300, 350])
    pygame.display.flip()
    
        
    # -------- Main Program Loop -----------
    while not done:
        # --- Main event loop
        for event in pygame.event.get(): # User did something
            if event.type == pygame.QUIT: # If user clicked close
                done = True # Flag that we are done so we exit this loop
            elif event.type == pygame.constants.USEREVENT:
                pygame.mixer.music.load("background_music.wav")
                pygame.mixer.music.play()
                
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    boy.jump()
                if event.key == pygame.K_DOWN:
                    boy.fall() 
                if event.key == pygame.K_LEFT:
                    boy.x_playerSpeed = -4
                if event.key == pygame.K_RIGHT:
                    boy.x_playerSpeed = 4
                if event.key == pygame.K_p:
                    toggle_pause += 1
                                    
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_LEFT:
                    if boy.y_player > 0:
                        boy.y_playerSpeed = 2
                        boy.x_playerSpeed = 0
                    else:
                        boy.x_playerSpeed = 0
                if event.key == pygame.K_RIGHT:
                    if boy.y_player > 0:
                        boy.y_playerSpeed = 2
                        boy.x_playerSpeed = 0
                    else:
                        boy.x_playerSpeed = 0
                if event.key == pygame.K_UP:
                    if boy.y_player > 0:
                        boy.y_playerSpeed = 2
                    else:
                        boy.y_playerSpeed = 0 
                if event.key == pygame.K_DOWN:
                    if boy.y_player > 0:
                        boy.y_playerSpeed = 2
                    else:
                        boy.y_playerSpeed = 0
                        
            if toggle_pause % 2 == 0: # Checks for game paused
                all_sprites_list.update()

        #Sets the background image to the screen  moving at speed -6
        
        #x_backgroundSpeed
        if x_background < -800:
          x_background = 0
          screen.blit(background_image, [0, y_background])
          screen.blit(background_image, [800, y_background])
          
        else:
          screen.blit(background_image, [x_background, y_background])
          screen.blit(background_image, [x_background + 799, y_background])
        x_background += x_backgroundSpeed
        switch_count += 4
        if switch_count > 1600:
          platform1.reset_pos()
          platform2.reset_pos()
          platform3.reset_pos()
          platform4.reset_pos()
          switch_count = 0 
        
      
        # --- Game logic should go here
        start_time = pygame.time.get_ticks()
        start_time = (start_time/1000)
        font_time = pygame.font.SysFont('Calibri', 20, True, False)
        text_time = font_time.render("Time: " + str((start_time)) + " seconds",True,(150,225,225))
        screen.blit(text_time, [610, 40])
        if toggle_pause % 2 == 0:        
            boy.rect.x = boy.x_player
            boy.rect.y = boy.y_player
            
        if toggle_pause % 2 == 0:
                all_sprites_list.update()
                font = pygame.font.SysFont('Calibri', 17, True, False)
                text = font.render("Press P to pause/unpause",True,(225,0,150))
                screen.blit(text, [610, 10])

        # Checks if the two parameters in the bracket collide with each other, true if it does        
        hit1 = pygame.sprite.collide_rect(platform1, boy)    
        if hit1: #If the objects collide, make the following changes
            # Walk in from top to bottom
            if (boy.y_player + 94) >= (platform1.rect.y - 5) and (boy.y_player + 94) <= (platform1.rect.y + 20) :
                boy.y_player = platform1.rect.y - 94
            # walk in from bottom to top    
            if boy.y_player <= (platform1.rect.y + 50) and boy.y_player >= (platform1.rect.y + 25):
                boy.y_player = platform1.rect.y + 44

            # Walk in from right to left    
            if (boy.x_player + 84) >= (platform1.rect.x - 5 ) and (boy.x_player + 84) <= (platform1.rect.x + 20):
                boy.x_player = platform1.rect.x - 84
            # Walk in from left to right    
            if boy.x_player <= (platform1.rect.x + 134) and boy.x_player >= (platform1.rect.x + 104):
                boy.x_player = platform1.rect.x + 130
                
        hit2 = pygame.sprite.collide_rect(platform2, boy)
        if hit2:
            # Walk in from top to bottom
            if (boy.y_player + 94) >= (platform2.rect.y - 5) and (boy.y_player + 94) <= (platform2.rect.y + 20) :
                boy.y_player = platform2.rect.y - 94
            # walk in from bottom to top    
            if boy.y_player <= (platform2.rect.y + 50) and boy.y_player >= (platform2.rect.y + 25):
                boy.y_player = platform2.rect.y + 44

            # Walk in from right to left    
            if (boy.x_player + 84) >= (platform2.rect.x - 5 ) and (boy.x_player + 84) <= (platform2.rect.x + 20):
                boy.x_player = platform2.rect.x - 84
            # Walk in from left to right    
            if boy.x_player <= (platform2.rect.x + 134) and boy.x_player >= (platform2.rect.x + 104):
                boy.x_player = platform2.rect.x + 130
                
        hit3 = pygame.sprite.collide_rect(platform3, boy)
        if hit3:
            # Walk in from top to bottom
            if (boy.y_player + 94) >= (platform3.rect.y - 5) and (boy.y_player + 94) <= (platform3.rect.y + 20) :
                boy.y_player = platform3.rect.y - 94
            # walk in from bottom to top    
            if boy.y_player <= (platform3.rect.y + 50) and boy.y_player >= (platform3.rect.y + 25):
                boy.y_player = platform3.rect.y + 44

            # Walk in from right to left    
            if (boy.x_player + 84) >= (platform3.rect.x - 5 ) and (boy.x_player + 84) <= (platform3.rect.x + 20):
                boy.x_player = platform3.rect.x - 84
            # Walk in from left to right    
            if boy.x_player <= (platform3.rect.x + 134) and boy.x_player >= (platform3.rect.x + 104):
                boy.x_player = platform3.rect.x + 130
                
        hit4 = pygame.sprite.collide_rect(platform4, boy)
        if hit4:
            # Walk in from top to bottom
            if (boy.y_player + 94) >= (platform4.rect.y - 5) and (boy.y_player + 94) <= (platform4.rect.y + 20) :
                boy.y_player = platform4.rect.y - 94
            # walk in from bottom to top    
            if boy.y_player <= (platform4.rect.y + 50) and boy.y_player >= (platform4.rect.y + 25):
                boy.y_player = platform4.rect.y + 44

            # Walk in from right to left    
            if (boy.x_player + 84) >= (platform4.rect.x - 5 ) and (boy.x_player + 84) <= (platform4.rect.x + 20):
                boy.x_player = platform4.rect.x - 84
            # Walk in from left to right    
            if boy.x_player <= (platform4.rect.x + 134) and boy.x_player >= (platform4.rect.x + 104):
                boy.x_player = platform4.rect.x + 130
                
        #hit5 = pygame.sprite.collide_rect(platform5, boy)
        '''if hit5:
            # Walk in from top to bottom
            if (boy.y_player + 94) >= (platform5.rect.y - 5) and (boy.y_player + 94) <= (platform5.rect.y + 20) :
                boy.y_player = platform5.rect.y - 102
            # walk in from bottom to top    
            if boy.y_player <= (platform5.rect.y + 50) and boy.y_player >= (platform5.rect.y + 25):
                boy.y_player = platform5.rect.y + 48

            # Walk in from right to left    
            if (boy.x_player + 84) >= (platform5.rect.x - 5 ) and (boy.x_player + 84) <= (platform5.rect.x + 20):
                boy.x_player = platform5.rect.x - 92
            # Walk in from left to right    
            if boy.x_player <= (platform5.rect.x + 134) and boy.x_player >= (platform5.rect.x + 104):
                boy.x_player = platform5.rect.x + 136 '''

        # Checks for collisions and changes the game_score accordingly
        blocks_hit_list_flame = pygame.sprite.spritecollide(boy, block_list_flame, False)
        if toggle_pause % 2 == 0:
            for blocks in blocks_hit_list_flame:
                # Reset block to the top of the screen to fall again.
                fire_sound.play()
                game_score -= 0.04 #value = 0.04
            
        blocks_hit_list = pygame.sprite.spritecollide(boy, block_list, False)
        if toggle_pause % 2 == 0:
            for block in blocks_hit_list:
                game_score += 0.15 # Set Value to 0.15              
                # Reset block to the side of the screen to fall again.
                block.reset_pos()

            
        game_score = game_score    
        font = pygame.font.SysFont('Calibri', 25, True, False)
        textScore = font.render("SCORE:",True,GREEN)
        text = font.render(str(float(game_score)),True,(10,210,225))
           
        screen.blit(textScore, [5, 5])
        screen.blit(text, [85, 5])
                  
        # Draw all the spites
        all_sprites_list.draw(screen)
        
        if toggle_pause % 2 == 0:        
            boy.x_player += boy.x_playerSpeed
            boy.y_player += boy.y_playerSpeed
        
        # Game_score = 0 and game ends if you hit the boundary 
        if boy.x_player <= -30 :
            boy.x_player = -15
            game_end(game_score, start_time)            
            game_score = 0
            toggle_pause += 1
            
        if boy.y_player <= -20:
            boy.y_player = -10
            game_end(game_score, start_time)            
            game_score = 0
            toggle_pause += 1

        if boy.y_player >= 475:
            boy.y_player = 200
            boy.x_player = 100
            game_end(game_score, start_time)            
            game_score = 0
            toggle_pause += 1       

        if boy.x_player >= 750:
            boy.x_player = 740
            game_end(game_score, start_time)            
            game_score = 0
            toggle_pause += 1

        # Displays the player image on the screen
        screen.blit(boy.player_image,[boy.x_player , boy.y_player])

        # Vaires the health bar depending on the score. Indicates full health and no health
        health_status = 200 + (game_score * 40)
        if health_status < 0:
            game_score = 0
            game_end(game_score, start_time)            
            game_score = 0
            toggle_pause += 1
           
            
        elif health_status > 399:
            health_status = 400
            health_bar_red = pygame.draw.rect(screen, RED, (200,10,400,15), 0)
            health_bar_green = pygame.draw.rect(screen, GREEN, (200,10,400,15), 0)
            font = pygame.font.SysFont('Calibri', 35, True, False)
            text = font.render("FULL HEALTH",True,(10, 210, 225, 125))
            screen.blit(text, [300, 10])
            
        else:
            health_status = 200 + (game_score * 40)
            health_bar_red = pygame.draw.rect(screen, RED, (200,10,400,15), 0)
            health_bar_green = pygame.draw.rect(screen, GREEN, (200,10,health_status,15), 0)
         
        # --- Go ahead and update the screen with what we've drawn.
        if toggle_pause % 2 == 0:
            pygame.display.flip()
     
        # --- Limit to 60 frames per second
        clock.tick(60)
     
    # Close the window and quit.

    pygame.quit()
else:
    font = pygame.font.SysFont('Calibri', 30, True, False)
    text = font.render("INCORRECT PASSWORD!!",True,(225,0,0))
    screen.blit(text, [300, 350])
    pygame.display.flip()
    pygame.time.wait(2500)
    pygame.quit()
    
